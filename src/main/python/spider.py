# -*- coding:utf-8 -*-

import requests
from bs4 import BeautifulSoup
import xlrd
import openpyxl
import warnings
import csv
import sys


reload(sys)
sys.setdefaultencoding("utf-8")

# 忽略python警告输出
warnings.filterwarnings("ignore")


def csvtoexcel():
    print u"开始转移csv文件数据至excel表中"
    # 读取2.csv文件
    csv_header = csv.reader(open("2.csv"))
    data = openpyxl.load_workbook("1.xlsx")
    try:
        if data.get_sheet_by_name("buglist"):
            data.remove_sheet(data.get_sheet_by_name("buglist"))
    except Exception:
        pass
    data.create_sheet("buglist")
    table = data.get_sheet_by_name("buglist")

    for row in csv_header:
        row_new = []
        # csv文件选取加入excel表的数组
        list = [0, 2, 4, 5, 6, 7, 8, 9, 10, 22, 11, 16, 15]
        for x in list:
            a = row[x].decode('gb2312')
            row_new.append(a)
        table.append(row_new)
    data.save('1.xlsx')



def readexcel():
    try:
        data = xlrd.open_workbook("1.xlsx")
        table = data.sheet_by_name("parameter")
    except Exception:
        print u"未找到excel文件或文件内未设置'parameter'sheet页"

    num_row = table.nrows
    a = {}
    for x in range(num_row):
        cell_1 = table.cell_value(x, 0)
        cell_2 = table.cell_value(x, 1)
        a[cell_1] = cell_2
    return a


# 分析buglist
def excel_buglist():
    print u"开始分析填充excel buglist表数据"
    try:
        data = openpyxl.load_workbook("1.xlsx")
        table = data.get_sheet_by_name("buglist")
    except Exception:
        print u"未找到excel文件或文件内未设置'buglist'sheet页"
    col_e = table["e"]  # 主题
    col_b = table["b"]  # 跟踪
    col_j = table["j"]  # 缺陷来源
    num = len(col_e)

    for x in range(num):
        col_e_value = col_e[x].value
        if col_j[x].value is None:
            if "ios" in col_e_value or "IOS" in col_e_value:
                table["J"+str(x+1)] = "ios"
            if "Android" in col_e_value or u"安卓" in col_e_value:
                table["J"+str(x+1)] = "Android"
            if "H5" in col_e_value:
                table["J"+str(x+1)] = "H5"
            if u"后端" in col_e_value:
                table["J"+str(x+1)] = u"后端"

        if "sprint1" in col_e_value:
            table["n"+str(x+1)] = "sprint1"
        if "sprint2" in col_e_value:
            table["n"+str(x+1)] = "sprint2"
        if "sprint3" in col_e_value:
            table["n"+str(x+1)] = "sprint3"

    data.save("1.xlsx")
    col_n = table["n"]  # 所属sprint
    # 计算buglist缺陷数
    num_1 = 0
    num_2 = 0
    num_3 = 0
    num_4 = 0
    num_5 = 0
    num_6 = 0
    num_7 = 0
    num_8 = 0
    num_9 = 0
    num_10 = 0
    num_11 = 0
    num_12 = 0
    for x in range(num):
        if col_j[x].value is None:
            print u"单元格N%i缺陷类型未被填充,请检查主题是否没有标明缺陷类型"%(x+1)
        if col_n[x].value is None:
            print u"单元格N%i所属sprint为空,请检查主题是否没有标明sprint"%(x+1)
        if col_n[x].value == "sprint1":
            if col_j[x].value == "ios":
                num_1 += 1
            if col_j[x].value == "Android":
                num_2 += 1
            if col_j[x].value == "H5":
                num_3 += 1
            if col_j[x].value == u"后端":
                num_4 += 1
        if col_n[x].value == "sprint2":
            if col_j[x].value == "ios":
                num_5 += 1
            if col_j[x].value == "Android":
                num_6 += 1
            if col_j[x].value == "H5":
                num_7 += 1
            if col_j[x].value == u"后端":
                num_8 += 1
        if col_n[x].value == "sprint3":
            if col_j[x].value == "ios":
                num_9 += 1
            if col_j[x].value == "Android":
                num_10 += 1
            if col_j[x].value == "H5":
                num_11 += 1
            if col_j[x].value == u"后端":
                num_12 += 1
    table = data.get_sheet_by_name("count")
    table["I9"] = num_1  # sprint1 ios缺陷
    table["H9"] = num_2  # sprint1 安卓缺陷
    table["J9"] = num_3  # sprint1 H5缺陷
    table["K9"] = num_4  # sprint1 后端缺陷
    table["I10"] = num_5  # sprint2 ios缺陷
    table["H10"] = num_6  # sprint2 安卓缺陷
    table["J10"] = num_7  # sprint2 H5缺陷
    table["K10"] = num_8  # sprint2 后端缺陷
    table["I11"] = num_9  # sprint3 ios缺陷
    table["H11"] = num_10  # sprint3 安卓缺陷
    table["J11"] = num_11  # sprint3 H5缺陷
    table["K11"] = num_12  # sprint3 后端缺陷

    data.save("1.xlsx")
    data.close()


def writeexcel(d):

    data = openpyxl.load_workbook("1.xlsx")
    num_1 = 0  # sprint1安卓用例数
    num_2 = 0  # sprint2安卓用例数
    num_3 = 0  # sprint3安卓用例数
    num_4 = 0  # sprint1IOS用例数
    num_5 = 0  # sprint2IOS用例数
    num_6 = 0  # sprint3IOS用例数
    num_7 = 0  # sprint1H5用例数
    num_8 = 0  # sprint2H5用例数
    num_9 = 0  # sprint3H5用例数

    # 筛选爬取出来的数据
    for x in range(len(d.keys())):
        if "sprint1" in d.keys()[x] and (u"安卓" in d.keys()[x] or "Android" in d.keys()[x]):
            num_1 += d.values()[x]
        if "sprint2" in d.keys()[x] and (u"安卓" in d.keys()[x]or "Android" in d.keys()[x]):
            num_2 += d.values()[x]
        if "sprint3" in d.keys()[x] and (u"安卓" in d.keys()[x] or "Android" in d.keys()[x]):
            num_3 += d.values()[x]
        if "sprint1" in d.keys()[x] and ("IOS" in d.keys()[x] or "ios" in d.keys()[x]):
            num_4 += d.values()[x]
        if "sprint2" in d.keys()[x] and ("IOS" in d.keys()[x] or "ios" in d.keys()[x]):
            num_5 += d.values()[x]
        if "sprint3" in d.keys()[x] and ("IOS"  in d.keys()[x] or "ios" in d.keys()[x]):
            num_6 += d.values()[x]
        if "sprint1" in d.keys()[x] and "H5" in d.keys()[x]:
            num_7 += d.values()[x]
        if "sprint2" in d.keys()[x] and "H5" in d.keys()[x]:
            num_8 += d.values()[x]
        if "sprint3" in d.keys()[x] and "H5" in d.keys()[x]:
            num_9 += d.values()[x]
    table = data.get_sheet_by_name("count")
    table["H2"] = num_1  # 写入sprint1安卓用例数
    table["H3"] = num_2  # 写入sprint2安卓用例数
    table["H4"] = num_3  # 写入sprint3安卓用例数
    table["I2"] = num_4  # 写入sprint1IOS用例数
    table["I3"] = num_5  # 写入sprint2IOS用例数
    table["I4"] = num_6  # 写入sprint3IOS用例数
    table["J2"] = num_7  # 写入sprint1H5用例数
    table["J3"] = num_8  # 写入sprint2H5用例数
    table["J4"] = num_9  # 写入sprint3H5用例数

    # 初始化data页
    try:
        if data.get_sheet_by_name("data"):
            data.remove_sheet(data.get_sheet_by_name("data"))
    except Exception:
        pass
    data.create_sheet("data")
    table = data.get_sheet_by_name("data")
    table.append(d.keys())
    table.append(d.values())
    data.save("1.xlsx")
    data.close()


def request(url):
    res = requests.get(url)
    content = res.content
    soup = BeautifulSoup(content)

    return soup


def one(user_name, versionnum):
    url = "http://zentao.beibei.com/zentao/user-testtask-%s.html" % user_name
    soup = request(url)
    tag = soup.find_all('tr', class_='text-center')
    version_num = len(tag)
    print u"......一共 %i 模块....." % version_num
    print u"......请输入爬取前几个模块....."
    # 校验下输入的模块数

    """
    while True:
        print u"请输入不大于%i且大于0的数目:" % version_num
        spider_num = int(raw_input())
        if 0 < spider_num <= version_num:
            break
        else:
            print u"校验数目失败"
    """
    b = []
    c = []
    for x in tag:
        if versionnum in x.find_all('td', class_='text-left nobr')[0].a.string:
            c.append(x.find_all('td', class_='text-left nobr')[0].a.string)
            b.append(x.td.a.string)

    return [b, c]


def two(num):
    url = "http://zentao.beibei.com/zentao/testtask-cases-%s.html" % num
    soup = request(url)

    tag = soup.find_all("td", class_="text-left nobr")
    """
    print "该组共有%i组用例"%len(tag)  注释下多少组用例以后用
    """
    b = []
    for a in tag:
        b.append(a.a["href"])
    return b


def three(html):
    url = "http://zentao.beibei.com" + html
    soup = request(url)

    tag = soup.find_all("th", class_="w-id text-center strong")
    """
    print "该组用例共有%i条用例"%len(tag) 注释下多少条用例以后用
    """
    return len(tag)


def main():
    print u"开始读取excel表parameter参数"
    data = readexcel()
    # 获取邮箱前缀
    usr_name = data[u"邮箱前缀"]
    verisonnum = data[u"版本号"]
    one_3 = one(usr_name, verisonnum)
    one_1 = one_3[0]
    one_2 = one_3[1]
    print u".........爬取中......."
    num_all = []
    for num in one_1:
        html = two(num)
        num_1 = 0
        for html_1 in html:
            num_2 = three(html_1)
            num_1 += num_2
        num_all.append(num_1)
    # 将爬取到的版本名和用例数合成一个字典d
    d = dict(map(None, one_2, num_all))
    for key in d:
        print key, ":", d[key]

    return d


def check():

    print u"检查是否存在1.xlsx文件"
    try:
        data = openpyxl.load_workbook("1.xlsx")
        print u"检查通过"
    except Exception:
        print u"1.xlsx文件不存在,请添加"
        return False
    print u"检查是否存在2.csv文件"
    try:
        data = open("2.csv")
        data.close()
        print u"检查通过"
    except Exception:
        print u"2.csv文件不存在,请添加"
        return False
    return True
if __name__ == '__main__':
    """
    main()
    print u"......爬取结束......"
    print u"....点击任意键退出......"
    raw_input()
    readexcel()
    """
    if check():
        csvtoexcel()
        excel_buglist()
        d = main()
        print u"资料写入excel data临时表"
        writeexcel(d)
        print u"....点击回车键退出......"
        raw_input()
    else:
        print u"检查不通过"
        print u"....点击回车键退出......"
        raw_input()






